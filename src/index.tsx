import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { defineCustomElements } from '@ionic/pwa-elements/loader';
import "@codetrix-studio/capacitor-google-auth";

defineCustomElements(window);

ReactDOM.render(<App/>, document.getElementById('root'));

