import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToggle,
  IonToolbar
} from '@ionic/react';
import {
  book,
  camera,
  help,
  home,
  informationCircle,
  logIn,
  logOut,
  map,
  person,
  personAdd,
  qrScanner
} from 'ionicons/icons';
import React, {useState} from 'react';
import {connect} from '../data/connect';
import {RouteComponentProps, withRouter} from 'react-router';
import {setDarkMode} from '../data/user/user.actions';

const routes = {
  appPages: [
    {title: 'Home', path: '/tabs/home', icon: home},
    {title: 'News', path: '/tabs/news', icon: book},
    {title: 'Camera', path: '/tabs/camera', icon: camera},
    {title: 'QR Scanner', path: '/tabs/scan', icon: qrScanner},
    {title: 'Map', path: '/tabs/map', icon: map},
    {title: 'About', path: '/tabs/about', icon: informationCircle}
  ],
  loggedInPages: [
    {title: 'Account', path: '/account', icon: person},
    {title: 'Support', path: '/support', icon: help},
    {title: 'Logout', path: '/logout', icon: logOut}
  ],
  loggedOutPages: [
    {title: 'Login', path: '/login', icon: logIn},
    {title: 'Support', path: '/support', icon: help},
    {title: 'Sign Up', path: '/signup', icon: personAdd}
  ]
};

interface Pages {
  title: string,
  path: string,
  icon: { ios: string, md: string },
  routerDirection?: string
}

interface StateProps {
  darkMode: boolean;
  isAuthenticated: boolean;
}

interface DispatchProps {
  setDarkMode: typeof setDarkMode
}

interface MenuProps extends RouteComponentProps, StateProps, DispatchProps {
}

const Menu: React.FC<MenuProps> = ({darkMode, history, isAuthenticated, setDarkMode}) => {
  const [disableMenu] = useState(false);

  function renderlistItems(list: Pages[]) {
    return list
      .filter(route => !!route.path)
      .map(p => (
        <IonMenuToggle key={p.title} auto-hide="false">
          <IonItem button routerLink={p.path} routerDirection="none">
            <IonIcon slot="start" icon={p.icon}/>
            <IonLabel>{p.title}</IonLabel>
          </IonItem>
        </IonMenuToggle>
      ));
  }


  return (
    <IonMenu type="overlay" disabled={disableMenu} contentId="main">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Menu</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent class="outer-content">

        <IonList>
          <IonListHeader>Navigate</IonListHeader>
          {renderlistItems(routes.appPages)}
        </IonList>
        <IonList>
          <IonListHeader>Account</IonListHeader>
          {isAuthenticated ? renderlistItems(routes.loggedInPages) : renderlistItems(routes.loggedOutPages)}
        </IonList>

        <IonList>
          <IonItem>
            <IonLabel>Dark Theme</IonLabel>
            <IonToggle checked={darkMode} onClick={() => setDarkMode(!darkMode)}/>
          </IonItem>
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default connect<{}, StateProps, {}>({
  mapStateToProps: (state) => ({
    darkMode: state.user.darkMode,
    isAuthenticated: state.user.isLoggedin
  }),
  mapDispatchToProps: ({
    setDarkMode
  }),
  component: withRouter(Menu)
})
