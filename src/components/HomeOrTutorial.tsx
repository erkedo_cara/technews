import React from 'react';
import { connect } from '../data/connect';
import { Redirect } from 'react-router';



const HomeOrTutorial: React.FC = () => {
  return  <Redirect to="/tabs/Home" />
};

export default connect<{}, {}>({
  mapStateToProps: (state) => ({

  }),
  component: HomeOrTutorial
});
