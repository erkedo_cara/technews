import React from 'react';
import {IonItem, IonLabel, IonList} from '@ionic/react';

interface AboutPopoverProps {
  dismiss: () => void;
}

const AboutPopover: React.FC<AboutPopoverProps> = ({dismiss}) => {

  const close = (url: string) => {
    window.open(url, '_blank');
    dismiss();
  };

  return (
    <IonList>
      <IonItem button onClick={() => close('https://reactjs.org/tutorial/tutorial.html')}>
        <IonLabel>Learn React</IonLabel>
      </IonItem>
      <IonItem button onClick={() => close('https://reactjs.org/docs/getting-started.html')}>
        <IonLabel>Documentation</IonLabel>
      </IonItem>

      <IonItem button onClick={() => close('https://github.com/facebook/react/')}>
        <IonLabel>GitHub Repo</IonLabel>
      </IonItem>
      <IonItem button href='/support'>
        <IonLabel>Support</IonLabel>
      </IonItem>
    </IonList>
  )
};

export default AboutPopover;
