import React, {Component} from 'react';
import axios from 'axios';
import './About.scss';
import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar
} from "@ionic/react";


function doRefresh(event) {
  console.log('Pull Event Triggered!');
  setTimeout(() => {
    console.log('Async operation has ended');

    //complete()  signify that the refreshing has completed and to close the refresher
    event.target.complete();
  }, 1000);
}

class Activity extends Component {


  API_URL = `https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=1c38b5833d974dacb123a2ca5dddd56a`;
  state = {

    searchActive: false,

    items: []
  };


  componentDidMount() {
    axios.get(this.API_URL).then(response => response.data)
      .then((data) => {
        this.setState({items: data.articles});
        console.log(this.state.items)
      })
  }

  render() {
    return (
      <IonPage>

        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton color="dark"/>
            </IonButtons>
            <IonTitle>News</IonTitle>

          </IonToolbar>

        </IonHeader>
        <IonContent>

          <IonRefresher id="ion-refresher-content" slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={50}
                        pullMax={100}>
            <IonRefresherContent id="ion-refresher-content"
            />
          </IonRefresher>
          {this.state.items.map((item) => (
            <IonCard>
              <img src={item.urlToImage}/>
              <IonCardHeader>
                <IonCardTitle>
                  {item.title}
                </IonCardTitle>
                <IonCardSubtitle>
                  {item.author}
                </IonCardSubtitle>
              </IonCardHeader>
              <IonCardContent>
                <p>{item.content}</p>
                <IonButton href={item.url}> Read</IonButton>
              </IonCardContent>
            </IonCard>
          ))}
        </IonContent>
      </IonPage>

    );
  }
}

export default Activity;
