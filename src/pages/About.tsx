import React, {useState} from 'react';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonDatetime,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonPopover,
  IonRefresher,
  IonRefresherContent,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import './About.scss';
import {calendar, more, pin} from 'ionicons/icons';
import AboutPopover from '../components/AboutPopover';
import {RefresherEventDetail} from "@ionic/core";

interface AboutProps {
}

function doRefresh(event: CustomEvent<RefresherEventDetail>) {
  console.log('Begin async operation');

  setTimeout(() => {
    console.log('Async operation has ended');
    event.detail.complete();
  }, 1000);
}

const About: React.FC<AboutProps> = () => {

  const [showPopover, setShowPopover] = useState(false);
  const [popoverEvent, setPopoverEvent] = useState();

  const presentPopover = (e: React.MouseEvent) => {
    setPopoverEvent(e.nativeEvent);
    setShowPopover(true);
  };
  const conferenceDate = '2047-05-17';

  return (
    <IonPage id="about-page">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton></IonMenuButton>
          </IonButtons>
          <IonTitle>About</IonTitle>
          <IonButtons slot="end">
            <IonButton icon-only onClick={presentPopover}>
              <IonIcon slot="icon-only" icon={more}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonRefresher id="ion-refresher-content" slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={50}
                      pullMax={100}>
          <IonRefresherContent id="ion-refresher-content"
          />
        </IonRefresher>
        <div className="about-header">
          <img src="assets/img/tech.png" alt="Ionic logo"/>
        </div>
        <div className="about-info">
          <h4 className="ion-padding-start">React and Mobile App Conference</h4>

          <IonList lines="none">
            <IonItem>
              <IonIcon icon={calendar} slot="start"></IonIcon>
              <IonLabel position="stacked">Date</IonLabel>
              <IonDatetime displayFormat="MMM DD, YYYY" max="2056" value={conferenceDate}></IonDatetime>
            </IonItem>

            <IonItem>
              <IonIcon icon={pin} slot="start"></IonIcon>
              <IonLabel position="stacked">Location</IonLabel>
              <IonSelect>
                <IonSelectOption value="madison" selected>Tirane</IonSelectOption>
                <IonSelectOption value="austin">Durres</IonSelectOption>
                <IonSelectOption value="chicago">Shkoder</IonSelectOption>
                <IonSelectOption value="seattle">Elbasan</IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonList>

          <p className="ion-padding-start ion-padding-end">
            This application is made only for education propose by Erkedo Cara.
            The React Conference is a one-day conference featuring talks for the React.
            It is focused on React applications etc
          </p>
        </div>
      </IonContent>
      <IonPopover
        isOpen={showPopover}
        event={popoverEvent}
        onDidDismiss={() => setShowPopover(false)}
      >
        <AboutPopover dismiss={() => setShowPopover(false)}/>
      </IonPopover>
    </IonPage>
  );
};

export default React.memo(About);
