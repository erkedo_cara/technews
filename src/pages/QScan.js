import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import React from 'react';
import './Home.css';
import QrReader from 'react-qr-reader'
import {logoChrome} from "ionicons/icons";

export class QScan extends React.Component {

  state = {
    result: "No result please wait,keep camera into the image ...",
    scanner: false
  };

  handleScan = data => {
    if (data) {
      this.setState({
        result: data
      })
    }
  };
  handleError = err => {
    console.error(err)
  };

  click = () => {
    if (this.state.result.startsWith("www")) {
      window.open(
        "https://" + this.state.result,
        '_blank' // <- This is what makes it open in a new window.
      );
    } else if (this.state.result.startsWith("http")) {
      window.open(
        this.state.result,
        '_blank' // <- This is what makes it open in a new window.
      );
    } else {
      window.open(
        'https://www.google.com/search?q=' + this.state.result,
        '_blank' // <- This is what makes it open in a new window.
      );
    }
  };

  render() {

    return <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton color="dark"/>
          </IonButtons>
          <IonTitle>QR Code Scanner</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonCard class="welcome-card">

          {!this.state.scanner && <img src="/assets/img/qrsc.jpg" alt=""/>}
          {this.state.scanner && <div>
            <QrReader
              delay={300}
              onError={this.handleError}
              onScan={this.handleScan}
              style={{width: '100%', border: 2}}
            />
            <IonCardContent>
              <IonItem onClick={this.click} target={"_blank"}>
                <IonIcon slot="start" color="medium" src={logoChrome}/>
                <p><a> {this.state.result}</a></p>
              </IonItem>
            </IonCardContent>
          </div>}

          {!this.state.scanner && <div>
            <IonCardHeader>
              <IonCardSubtitle>Get Started</IonCardSubtitle>
              <IonCardTitle> Scan QR Code </IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
              <p>Here we can scan a Qr code to get data</p>

            </IonCardContent>

          </div>}
          {this.state.scanner && <IonButton expand='block' onClick={() => this.setState({
            scanner: !this.state.scanner
          })}>Close QR Scanner</IonButton>}
          {!this.state.scanner && <IonButton expand='block' onClick={() => this.setState({
            scanner: !this.state.scanner
          })}>Click to scan</IonButton>}

        </IonCard>

      </IonContent>


    </IonPage>
  };
}

export default QScan;
