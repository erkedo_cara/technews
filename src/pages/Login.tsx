import React, {useState} from 'react';
import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import './Login.scss';
import {setIsLoggedIn, setUsername} from '../data/user/user.actions';
import {connect} from '../data/connect';
import {RouteComponentProps} from 'react-router';
import {contact, key, logoGoogle} from "ionicons/icons";

interface OwnProps extends RouteComponentProps {
}

interface DispatchProps {
  setIsLoggedIn: typeof setIsLoggedIn;
  setUsername: typeof setUsername;
}

interface LoginProps extends OwnProps, DispatchProps {
}

const Login: React.FC<LoginProps> = ({setIsLoggedIn, history, setUsername: setUsernameAction}) => {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [formSubmitted, setFormSubmitted] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const login = async (e: React.FormEvent) => {
    e.preventDefault();
    setFormSubmitted(true);
    if (!username) {
      setUsernameError(true);
    }
    if (!password) {
      setPasswordError(true);
    }

    if (username && password) {
      await setIsLoggedIn(true);
      await setUsernameAction(username);
      history.push('/tabs/home', {direction: 'none'});
    }
  };

  return (
    <IonPage id="login-page">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton></IonMenuButton>
          </IonButtons>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonRow/>

        <div className="login-logo">
          <img src="assets/img/tech.png" alt="Ionic logo"/>
        </div>

        <form noValidate onSubmit={login}>
          <IonList>
            <IonItem>

              <IonIcon slot="start" color={"primary"} size={"large"} icon={contact}></IonIcon>

              <IonInput name="username" placeholder={"Username"} type="email" value={username} spellCheck={false}
                        autocapitalize="off"
                        onIonChange={e => setUsername(e.detail.value!)}
                        required>
              </IonInput>
            </IonItem>

            {formSubmitted && usernameError && <IonText color="danger">
              <p className="ion-padding-start">
                Username is required
              </p>
            </IonText>}

            <IonItem>
              <IonIcon slot="start" color={"primary"} size={"large"} icon={key}></IonIcon>
              <IonInput name="password" placeholder={"Password"} type="password" value={password}
                        onIonChange={e => setPassword(e.detail.value!)} required>
              </IonInput>
            </IonItem>

            {formSubmitted && passwordError && <IonText color="danger">
              <p className="ion-padding-start">
                Password is required
              </p>
            </IonText>}
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton fill="outline" type="submit" expand="block">Login</IonButton>
            </IonCol>
            <IonCol>
              <IonButton routerLink="/signup" fill="outline" expand="block">Signup</IonButton>
            </IonCol>

          </IonRow>

          <IonRow>
            <IonCol>
              <IonButton routerLink="/login2" fill="outline" expand="block">

                <IonIcon slot="start" src={logoGoogle}/>
                <IonLabel>Login with Google</IonLabel>
              </IonButton>
            </IonCol>

          </IonRow>

        </form>


      </IonContent>

    </IonPage>
  );
};

export default connect<OwnProps, {}, DispatchProps>({
  mapDispatchToProps: {
    setIsLoggedIn,
    setUsername
  },
  component: Login
})
