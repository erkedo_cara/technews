import React from 'react';
import {IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs} from '@ionic/react';
import {Redirect, Route} from 'react-router';
import {book, camera, home} from 'ionicons/icons';
import MapView from './MapView';
import About from './About';
import Home from './Home'
import Activity from "./Activity";
import Camera from "./Camera";
import QScan from "./QScan";

interface MainTabsProps {
}

const MainTabs: React.FC<MainTabsProps> = () => {

  return (
    <IonTabs>
      <IonRouterOutlet>
        <Redirect exact path="/tabs" to="/tabs/home"/>
        {/*
          Using the render method prop cuts down the number of renders your components will have due to route changes.
          Use the component prop when your component depends on the RouterComponentProps passed in automatically.
        */}
        <Route path="/tabs/home" render={() => <Home/>} exact={true}/>
        <Route path="/tabs/news" render={() => <Activity/>} exact={true}/>
        <Route path="/tabs/map" render={() => <MapView/>} exact={true}/>
        <Route path="/tabs/camera" render={() => <Camera/>} exact={true}/>
        <Route path="/tabs/scan" render={() => <QScan/>} exact={true}/>
        <Route path="/tabs/about" render={() => <About/>} exact={true}/>
      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton tab="home" href="/tabs/home">
          <IonIcon icon={home}/>
          <IonLabel>Home</IonLabel>
        </IonTabButton>
        <IonTabButton tab="news" href="/tabs/news">
          <IonIcon icon={book}/>
          <IonLabel>News</IonLabel>
        </IonTabButton>

        <IonTabButton tab="camera" href="/tabs/camera">
          <IonIcon icon={camera}/>
          <IonLabel>Camera</IonLabel>
        </IonTabButton>

      </IonTabBar>
    </IonTabs>
  );
};

export default MainTabs;
