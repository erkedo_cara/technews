import {
  IonAlert,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonImg,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import React, {Component} from 'react';
import './Login.scss';
import {Plugins} from '@capacitor/core';
import "@codetrix-studio/capacitor-google-auth";

const INITIAL_STATE = {};


class Login2 extends Component {
  state: any = {};
  props: any = {};

  constructor(props: any) {
    super(props);
    this.state = {
      ...INITIAL_STATE,
      showAlert1: false,
      setShowAlert1: false
    };
  }

  async signIn(): Promise<void> {
    const {history} = this.props;
    const result = await Plugins.GoogleAuth.signIn();
    console.info('result', result);
    if (result) {
      history.push({
        pathname: '/home2',
        state: {name: result.name || result.displayName, image: result.imageUrl, email: result.email}
      });
    }
  }

  render() {
    return (


      <IonPage id="login-page">

        <IonAlert
          isOpen={this.state.showAlert1}
          onDidDismiss={() => this.setState({
            setShowAlert1: false,
            showAlert1: false,
          })}
          header={'Privacy Alert!'}
          message={' <strong>Google will receive your name, profile picture and email address .</strong>!!!'}
          buttons={[
            {
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'secondary',
              handler: blah => {
                console.log('Confirm Cancel');
              }
            },
            {
              text: 'Allow!',
              handler: () => this.signIn()
            }
          ]}
        />
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonTitle>Google Authentication</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonContent>
          <IonCard>
            <IonCardHeader>

              <IonCardTitle>
                <IonImg id="test" src={"assets/img/google.png"}/>


              </IonCardTitle>

              <IonCardSubtitle>
              </IonCardSubtitle>
            </IonCardHeader>
            <IonCardContent>

              <p>{"Chances are, Google has your name, a photo of your face, your birthday, gender, other email\n" +
              "                addresses you use, your password and phone number. Some of this is listed as public information (not\n" +
              "                your password, of course). Here's how to see what Google shares with the world about you.\n" +
              "                Open a browser window and navigate to your your Google Account page.\n" +
              "                Type your Google username (with or without \"@gmail.com\").\n" +
              "                From the menu bar, choose Personal info and review the information. You can change or delete your photo,\n" +
              "                name, birthday, gender, password, other emails and phone number.\n" +
              "                If you'd like to see what information of yours is available publicly, scroll to the bottom and select Go\n" +
              "                to About me.\n" +
              "               "}</p>
              <br>
              </br>
              <IonButton className="login-button" onClick={() => this.setState({
                showAlert1: true,

              })} expand="block" fill="solid"

                         color="danger">
                Login with Google
              </IonButton>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonPage>
    )
  }
}

export default Login2;
