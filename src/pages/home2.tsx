import {
  IonButton,
  IonCard,
  IonCardHeader,
  IonCol,
  IonContent,
  IonHeader,
  IonPage, IonRefresher, IonRefresherContent,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import React, {Component} from 'react';
import './Login.scss';
import {Plugins} from '@capacitor/core';
import "@codetrix-studio/capacitor-google-auth";
import {RefresherEventDetail} from "@ionic/core";

const INITIAL_STATE = {
  loggedIn: true,
  user: {}
};

function doRefresh(event: CustomEvent<RefresherEventDetail>) {
  console.log('Begin async operation');
  setTimeout(() => {
    console.log('Async operation has ended');
    event.detail.complete();

  }, 1000);

}

class Home2 extends Component {
  state: any = {};
  props: any = {};

  constructor(props: any) {
    super(props);
    this.state = {...INITIAL_STATE};
  }

  componentDidMount() {
    this.getUserInfo();
  }

  async signOut(): Promise<void> {
    const {history} = this.props;
    await Plugins.GoogleAuth.signOut();
    history.goBack();
  }

  async getUserInfo() {
    this.setState({
      user: {
        name: this.props.location.state.name,
        image: this.props.location.state.image,
        email: this.props.location.state.email
      }
    })
  }

  render() {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar color="primary">
            <IonTitle>Logged in ... </IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonContent className="ion-padding">
          <IonRefresher slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={50} pullMax={100}>
            <IonRefresherContent id="ion-refresher-content"></IonRefresherContent>
          </IonRefresher>
          <IonRow>
            <IonCol className="text-center">
              <IonText className="title">
                Your Image!
              </IonText>
            </IonCol>
          </IonRow>

          <IonCard className="welcome-card">
            <img src={this.state.user.image} alt="Google Account Image"/>
            <IonCardHeader>
              Here are your google account information
            </IonCardHeader>

            <IonText color="primary">
              <h5>  {"Name:" + this.state.user.name}
                <br></br>
                {"Email:" + this.state.user.email}</h5>
            </IonText>
          </IonCard>
          <IonButton className="login-button" onClick={() => this.signOut()} expand="full" fill="solid" color="danger">
            Logout from Google
          </IonButton>
        </IonContent>
      </IonPage>
    )
  }
}

export default Home2;
