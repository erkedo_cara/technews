import {
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList, IonMenuButton,
  IonPage, IonProgressBar,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import { camera, trash, close } from 'ionicons/icons';
import {IonFab, IonFabButton, IonIcon, IonGrid, IonRow,
  IonCol, IonImg, IonActionSheet } from '@ionic/react';

import { usePhotoGallery, Photo } from '../components/usePhotoGallery';
import React, { useState } from 'react';


const Camera: React.FC = () => {
  const { photos, takePhoto,deletePhoto } = usePhotoGallery();
  const [photoToDelete, setPhotoToDelete] = useState<Photo>();



  return (

    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton color="dark"/>
          </IonButtons>
          <IonTitle>Photo Gallery</IonTitle>

        </IonToolbar>
      </IonHeader>


      <IonContent>
        <IonGrid>
          <IonProgressBar type="indeterminate" reversed={true}></IonProgressBar><br/>
          <IonRow>
            {photos.map((photo, index) => (
              <IonCol size="6" key={index}>
                <IonImg onClick={() => setPhotoToDelete(photo)} src={photo.base64 !=null ? photo.base64 : photo.webviewPath}/>
              </IonCol>
            ))}
             <IonActionSheet
               isOpen={!!photoToDelete}
               buttons={[{
                text: 'Delete',
                 role: 'destructive',
                icon: 'trash',
                handler: () => {
                   if (photoToDelete) {
                     deletePhoto(photoToDelete);
                   setPhotoToDelete(undefined);
                  }
                 }
               }, {
                 text: 'Cancel',
                 icon: 'close',
                 role: 'cancel',

              }]}
              onDidDismiss={() => setPhotoToDelete(undefined)}
             />
          </IonRow>
        </IonGrid>

          <IonFab vertical="bottom" horizontal="center" slot="fixed">
            <IonFabButton  onClick={() => takePhoto()}>
              <IonIcon icon={camera}></IonIcon>
            </IonFabButton>
          </IonFab>

      </IonContent>

    </IonPage>
  );
};

export default Camera;


