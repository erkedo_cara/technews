import {
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonHeader,
  IonIcon,
  IonImg,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenuButton,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import {
  logoAndroid,
  logoApple,
  logoChrome,
  logoEuro,
  logoFacebook,
  logoGoogleplus,
  logoReddit,
  logoTumblr,
  logoTwitter,
  logoVimeo,
  logoWindows,
  share
} from 'ionicons/icons';
import {RefresherEventDetail} from '@ionic/core';
import React from 'react';
import './Home.css';


function doRefresh(event: CustomEvent<RefresherEventDetail>) {
  console.log('Begin async operation');


  setTimeout(() => {
    console.log('Async operation has ended');
    event.detail.complete();

  }, 1000);

}

const HomePage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton color="dark"/>
          </IonButtons>
          <IonTitle>Home</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>

        {/*-- Default Refresher --*/}
        <IonRefresher slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={50} pullMax={100}>
          <IonRefresherContent id="ion-refresher-content"></IonRefresherContent>
        </IonRefresher>
        <IonCard className="welcome-card">
          <IonImg
            src="assets/img/homeImage.jpg" alt="Ionic logo"/>
          <IonCardHeader>
            <IonCardTitle>Welcome to Tech News</IonCardTitle>
          </IonCardHeader>
        </IonCard>
        <IonFab slot="fixed" vertical="bottom" horizontal="end">
          <IonFabButton>
            <IonIcon icon={share}/>
          </IonFabButton>
          <IonFabList side="top">
            <IonFabButton color="vimeo">
              <IonIcon icon={logoVimeo}/>
            </IonFabButton>
            <IonFabButton color="google">
              <IonIcon icon={logoGoogleplus}/>
            </IonFabButton>
            <IonFabButton color="twitter">
              <IonIcon icon={logoTwitter}/>
            </IonFabButton>
            <IonFabButton color="facebook">
              <IonIcon icon={logoFacebook}/>
            </IonFabButton>
          </IonFabList>
        </IonFab>
        <IonList lines="none">
          <IonListHeader>
            <IonLabel>Resources</IonLabel>
          </IonListHeader>
          <IonItem href="https://www.apple.com/apple-news/" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoApple}/>
            <IonLabel>Apple News</IonLabel>
          </IonItem>
          <IonItem href="https://news.microsoft.com/" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoWindows}/>
            <IonLabel>Microsoft</IonLabel>
          </IonItem>
          <IonItem href="https://www.theverge.com/tech" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoVimeo}/>
            <IonLabel>The Verge</IonLabel>
          </IonItem>
          <IonItem href="https://www.reddit.com/" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoReddit}/>
            <IonLabel>Reddit</IonLabel>
          </IonItem>
          <IonItem href="https://www.androidauthority.com/news/" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoAndroid}/>
            <IonLabel>Android Authority</IonLabel>
          </IonItem>
          <IonItem href="https://www.techradar.com/news/world-of-tech" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoTumblr}/>
            <IonLabel>TechRadar</IonLabel>
          </IonItem>
          <IonItem href="https://www.cnet.com/" target={"_blank"}>
            <IonIcon slot="start" color="medium" src={logoEuro}></IonIcon>
            <IonLabel>C|NET</IonLabel>
          </IonItem>
          <IonItem href="https://www.anandtech.com/" target={"_blank"}>
            <IonIcon slot="start" color="medium" icon={logoChrome}/>
            <IonLabel>AnandTech</IonLabel>
          </IonItem>
          <IonItem href="https://arstechnica.com/" target={"_blank"}>
            <IonIcon slot="start" color="medium" icon={logoChrome}></IonIcon>
            <IonLabel>Arstechnica</IonLabel>
          </IonItem>


        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default HomePage;
